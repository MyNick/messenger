#!C:\usr\Python\python
# -*- coding: utf-8 -*-

import os, sys, argparse, pickle, winsound, datetime
from PyQt4.QtGui import *
from PyQt4 import QtCore, QtGui, uic
from socket import *


def PlaySound(file):
    winsound.PlaySound(file, winsound.SND_ALIAS)

def infoBox(var):
    window = QtGui.QWidget()
    window.setWindowTitle(u"Класс QMessageBox")
    window.resize(300, 70)
    QtGui.QMessageBox.information(window, u"Инфо",u"Текст сообщения"+var,buttons=QtGui.QMessageBox.Close,defaultButton=QtGui.QMessageBox.Close)

def LoadSettings():
    import xml.etree.ElementTree as ET
    if not os.path.isfile('C:\\Messendger\\settings.xml'):
        print(u'Файл C:\\Messendger\\settings.xml не найден')
        #infoBox(u'Файл C:\\Messendger\\settings.xml не найден') - окно не появляется
        sys.exit()
        #QtGui.QMessageBox.information(self, u"Ошибка",u"Файл C:\\Messendger\\settings.xml не найден",buttons=QtGui.QMessageBox.Close,defaultButton=QtGui.QMessageBox.Close)
    tree = ET.parse('C:\\Messendger\\settings.xml')
    root = tree.getroot()
    settings = {}
    for child in root:
        settings[child.tag] = child.text
    return settings

Settings=LoadSettings()

class MyThread(QtCore.QThread):
    def __init__(self, server, port, MaxBytes, parent=None):
        QtCore.QThread.__init__(self, parent)
        self.running = False # флаг выполнения
        self.server = server
        self.port = port
        self.MaxBytes = MaxBytes
    def run(self):
        #if not self.running:
        #  return

        #Settings=LoadSettings()

        sock = socket(AF_INET, SOCK_DGRAM)
        sock.bind((self.server, self.port))
        print('Listening at {}'.format(sock.getsockname()))
        
        #self.running = True
        while self.running:
            data, address = sock.recvfrom(self.MaxBytes)
            # Передача данных из потока через сигнал
            self.emit(QtCore.SIGNAL("mysignal(QString)"), "%s" % pickle.loads(data))

        sock.close()

        # остановка потока
    def stop(self):
        self.running = False
        #self.terminate()
        #self.wait() если раскомментировать - зависает

class Prog(QtGui.QWidget):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)

        uic.loadUi("views\\ui.ui", self)
        self.connect(self.settingsButton, QtCore.SIGNAL('clicked()'), self.WindowSettings)
        self.connect(self.button, QtCore.SIGNAL('clicked()'), self.sentMessage)
        self.connect(self.button2, QtCore.SIGNAL('clicked()'), self.on_stop)
        #window.show()

        self.mythread = MyThread(Settings['ServerIp'], int(Settings['ServerPort']), int(Settings['MaxBytes']))     # Создаем экземпляр класса
        #self.connect(self.button, QtCore.SIGNAL("clicked()"),
        #             self.on_clicked)
        self.connect(self.mythread, QtCore.SIGNAL("started()"),
                     self.on_started)
        self.connect(self.mythread, QtCore.SIGNAL("finished()"),
                     self.on_finished)
        self.connect(self.mythread, QtCore.SIGNAL("mysignal(QString)"),
                     self.on_change, QtCore.Qt.QueuedConnection)

    def WindowSettings(self):
        from window import MyWindow
        self.form = MyWindow("settings")
        self.form.show()
        self.form.serverIp.setText(Settings['ServerIp'])
        self.form.clientIp.setText(Settings['ClientIp'])
        self.form.serverPort.setText(Settings['ServerPort'])
        self.form.clientPort.setText(Settings['ClientPort'])
        self.form.maxBytes.setText(Settings['MaxBytes'])
        # setValidator физически не позволяет ввести неправильные данные
        self.form.serverPort.setValidator(QtGui.QIntValidator(0, 65535, self))
        self.form.clientPort.setValidator(QtGui.QIntValidator(0, 65535, self))
        self.form.maxBytes.setValidator(QtGui.QIntValidator(0, 65535, self))

    def on_stop(self):
        if self.mythread.isRunning():
            self.ChatWindow.setText(self.ChatWindow.toPlainText() + u'Остановка сервера...on_stop\n')
            self.mythread.stop()
            #self.mythread.running = False
            #if self.mythread.isRunning():
            #    self.ChatWindow.setText(self.ChatWindow.toPlainText() + u'Поток выполняется...\n')
            self.button2.setText(u'Подключить')
        else:
            self.mythread.start()
            self.mythread.running = True  # Изменяем флаг выполнения
            self.ChatWindow.setText(self.ChatWindow.toPlainText() + u'Запуск сервера...\n')
    def on_clicked(self):
        self.mythread.start()          # Запускаем поток
    def on_started(self):              # Вызывается при запуске потока
        #self.Line.setText("Вызван метод on_started()")
        self.ChatWindow.setText(self.ChatWindow.toPlainText() + u'Вызван метод on_started()\n')
        self.button2.setText(u'Отключить')
    def on_finished(self):             # Вызывается при завершении потока
        #self.Line.setText("Вызван метод on_finished()")
        self.ChatWindow.setText(self.ChatWindow.toPlainText() + u'Вызван метод on_finished()\n')
        self.button2.setText(u'Подключить')
    def on_change(self, s):
        self.ChatWindow.setText(self.ChatWindow.toPlainText() + s + '\n')
        PlaySound('IncMsg.wav')


    def sentMessage(self):
        #infoBox(Settings['ClientIp'])
        self.client(Settings['ClientIp'], int(Settings['ClientPort']))
        PlaySound('MsgSent.wav')

    def client(self, server, port):
        #MAX_BYTES = 65535
        sock = socket(AF_INET, SOCK_DGRAM)

        t = self.lineEdit.text()#type(t) -> class QString
        t = pickle.dumps(t)#сериализация объекта

        self.ChatWindow.setText(pickle.loads(t))
        sock.sendto(t, (server, port))


app = QtGui.QApplication(sys.argv)
#window = uic.loadUi("ui.ui")
#window.show()
prog = Prog()
prog.show()
sys.exit(app.exec_())
