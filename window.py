#!C:\usr\Python\python
# -*- coding: utf-8 -*-

from PyQt4 import QtCore, QtGui, uic
import pickle
class MyWindow(QtGui.QWidget):
    def __init__(self, viewName, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.viewName = viewName
        uic.loadUi("views\\" + self.viewName + ".ui", self)
        self.connect(self.saveButton, QtCore.SIGNAL("clicked()"),
                     self.SaveSettings)
        self.connect(self.cancelButton, QtCore.SIGNAL("clicked()"),
            QtCore.SLOT("close()"))
    def SaveSettings(self):
        if not self.valid_ip(str(self.serverIp.text())):
            QtGui.QMessageBox.information(self, u"Ошибка",u"Неверный IP Сервера",buttons=QtGui.QMessageBox.Close,defaultButton=QtGui.QMessageBox.Close)
            return
        elif not self.valid_ip(str(self.clientIp.text())):
            QtGui.QMessageBox.information(self, u"Ошибка",u"Неверный IP Клиента",buttons=QtGui.QMessageBox.Close,defaultButton=QtGui.QMessageBox.Close)
            return
        else:
            s = []
            for i in self.children():
                if str(type(i)) == "<class 'PyQt4.QtGui.QLineEdit'>":
                    #print str(unicode(i.text(), 'cp1251'))
                    #print i.text()
                    s.append(i.text())

            '''# способ 1 - создание простого нового текстового файла
            tree = """<?xml version=\"1.0\" encoding=\"utf-8\"?>
<AppSettings>
  <ServerIp>""" + s[0] + """</ServerIp>
  <ClientIp>""" + s[1] + """</ClientIp>
  <ServerPort>""" + s[2] + """</ServerPort>
  <ClientPort>""" + s[3] + """</ClientPort>
  <MaxBytes>""" + s[4] + """</MaxBytes>
</AppSettings>"""
            f = open("settings.xml", "w")
            f.write(tree) # если файл уже существует он будет перезаписан
            f.close()'''
            # способ 2 (лучший) - модификация существующего xml-файла средствами библиотеки ElementTree
            import xml.etree.ElementTree as ET
            tree = ET.parse('C:\\Messendger\\settings.xml')

            root = tree.getroot()
            i = 0
            for rank in root:
                #print rank.text
                rank.text = str(s[i])
                i += 1
            #root[1].text = str(self.clientIp.text()) - так можно сохранить способом 3, без цикла
            #write() Returns an encoded string. Как проверить сохранен ли файл?
            tree.write('C:\\Messendger\\settings.xml', xml_declaration=True, encoding='utf-8', method="xml")
#open("text2.txt", 'w')
            #tree.write('output.xml')
    def valid_ip(self, address):
        try:
            host_bytes = address.split('.')
            valid = [int(b) for b in host_bytes]
            valid = [b for b in valid if b >= 0 and b<=255]
            return len(host_bytes) == 4 and len(valid) == 4
        except:
            return False
